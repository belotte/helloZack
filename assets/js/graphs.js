$ (document).ready (function () {
	updateCharts ()
})

function updateCharts () {
	petsStatusChart ()
	petsPerDayChart ()
}

function petsStatusChart () {
	getRequest ('/api/v1/signalements', function (response) {
		var data = JSON.parse (response)
		var status = {}
		var labels = []
		var series = []

		for (var i in data) {
			if (!status[data[i].status]) {
				status[data[i].status] = 0
			}
			status[data[i].status] += 1
		}
		for (var i in status) {
			labels.push (`${i}${status[i] > 1 ? 's' : ''}`)
			series.push (status[i])
		}

		var data = {
			labels: labels,
			series: series,
		}

		var options = {
			labelInterpolationFnc: function (value) {
				return value[0]
			}
		}

		var responsiveOptions = [
			['screen and (min-width: 640px)', {
				chartPadding: 30,
				labelOffset: 10,
				// labelDirection: 'explode',
				labelInterpolationFnc: function(value) {
					return value;
				}
			}],
			['screen and (min-width: 1024px)', {
				labelOffset: 10,
				chartPadding: 10
			}]
		]

		new Chartist.Pie ('.petsStatus', data, options, responsiveOptions)

		setTimeout (function () {
			var i = 0
			while (++i < labels.length + 1) {
				$ (`.ct-chart-pie g.ct-series:nth-child(${i})`).addClass (labels[i - 1].substring (0, 2).toLowerCase ())
			}
		}, 1)
	})
}

function petsPerDayChart () {
	getRequest ('/api/v1/signalements', function (response) {
		var data = JSON.parse (response)
		var days = {}
		var labels = []
		var series = []

		for (var i in data) {
			var dateParts = data[i].date.split ('/')
			data[i].newDate = `${parseInt (dateParts[2]) > new Date ().getFullYear () % 100 ? '19' : '20'}${dateParts[2]}-${dateParts[1]}-${dateParts[0]}`
		}
		data = data.sort (function (a,b) {
			return new Date (a.newDate).getTime () > new Date (b.newDate).getTime () ? 1 : -1
		})

		for (var i in data) {
			if (!days[data[i].date]) {
				days[data[i].date] = 0
			}
			days[data[i].date] += 1
		}
		for (var i in days) {
			labels.push (i)
			series.push (days[i])
		}
		//
		// var data = {
		// 	labels: labels,
		// 	series: series,
		// }

		new Chartist.Line('.petsPerDay', {
			labels: labels,
			series: [
				series
			]
		}, {
			low: 0,
			showArea: true
		});
	})
}
