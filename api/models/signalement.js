var db =			require ('../services/db')
var log =			require ('../services/log')
var infos =			require ('../../config/infos')
var collection =	'signalement'

module.exports = {
	create: function (options, callback) {
		if (!options) {
			return callback (`signalement.create (options[, callback])`)
		} else if (!options.date || !/^[0-3][0-9]\/[0-1][0-9]\/[0-9]{2}$/.test (options.date)) {
			return callback (`options.date is '${options.date}', must be like '27/12/16'.`)
		} else if (!options.creneau || !/^[0-2][0-9]h-[0-2][0-9]h$/.test (options.creneau)) {
			return callback (`options.creneau is '${options.creneau}', must be like '10h-11h'.`)
		} else if (!options.alerteur || !/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test (options.alerteur)) {
			return callback (`options.alerteur is '${options.alerteur}', must be a mail.`)
		} else if (!options.animal || options.animal.length == 0 || infos.animaux.map (function (s) {return s.toLowerCase ()}).indexOf (options.animal.toLowerCase ()) < 0) {
			return callback (`options.animal is '${options.animal}', must be one of ${infos.animaux.toString ()}.`)
		} else if (!options.couleur || !/^[a-zA-Z]{2,20}$/.test (options.couleur)) {
			return callback (`options.couleur is '${options.couleur}', must be like black, grey.`)
		} else if (!options.adresse || !/^[a-zA-Z0-9 ,]+$/.test (options.couleur)) {
			return callback (`options.adresse is '${options.adresse}', must be like '96 Boulevard Bessière, 75017 Paris'.`)
		} else if (!options.etat || options.etat.length == 0 || infos.etats.map (function (s) {return s.toLowerCase ()}).indexOf (options.etat.toLowerCase ()) < 0) {
			return callback (`options.etat is '${options.etat}', must be one of ${infos.etats.toString ()}.`)
		}

		db.insert ({
			collection: collection,
			object: {
				date: options.date,
				creneau: options.creneau,
				alerteur: options.alerteur,
				animal: options.animal,
				couleur: options.couleur,
				adresse: options.adresse,
				etat: options.etat,
				collier: options.collier,
				status: options.status,
				brigade: options.brigade
			}
		}, callback)
	},

	find: function (options, callback) {
		db.find ({
			collection: collection,
			query: options.query,
			fields: options.fields
		}, callback)
	},

	update: function (options, callback) {
		if (options.newValues.date && !/^[0-3][0-9]\/[0-1][0-9]\/[0-9]{2}$/.test (options.newValues.date)) {
			return callback (`options.date is '${options.newValues.date}', must be like '27/12/16'.`)
		} else if (!options.newValues.date) {
			delete options.newValues.date
		}
		if (options.newValues.creneau && !/^[0-2][0-9]h-[0-2][0-9]h$/.test (options.newValues.creneau)) {
			return callback (`options.creneau is '${options.newValues.creneau}', must be like '10h-11h'.`)
		} else if (!options.newValues.creneau) {
			delete options.newValues.creneau
		}
		if (options.newValues.alerteur && !/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test (options.newValues.alerteur)) {
			return callback (`options.alerteur is '${options.newValues.alerteur}', must be a mail.`)
		} else if (!options.newValues.alerteur) {
			delete options.newValues.alerteur
		}
		if (options.newValues.animal && infos.animaux.map (function (s) {return s.toLowerCase ()}).indexOf (options.newValues.animal.toLowerCase ()) < 0) {
			return callback (`options.animal is '${options.newValues.animal}', must be one of ${infos.animaux.toString ()}.`)
		} else if (!options.newValues.animal) {
			delete options.newValues.animal
		}
		if (options.newValues.couleur && !/^[a-zA-Z]{2,20}$/.test (options.newValues.couleur)) {
			return callback (`options.couleur is '${options.newValues.couleur}', must be like black, grey.`)
		} else if (!options.newValues.couleur) {
			delete options.newValues.couleur
		}
		if (options.newValues.adresse && !/^[a-zA-Z0-9 ,]+$/.test (options.newValues.couleur)) {
			return callback (`options.adresse is '${options.newValues.adresse}', must be like '96 Boulevard Bessière, 75017 Paris'.`)
		} else if (!options.newValues.adresse) {
			delete options.newValues.adresse
		}
		if (options.newValues.etat && infos.etats.map (function (s) {return s.toLowerCase ()}).indexOf (options.newValues.etat.toLowerCase ()) < 0) {
			return callback (`options.etat is '${options.newValues.etat}', must be one of ${infos.etats.toString ()}.`)
		} else if (!options.newValues.etat) {
			delete options.newValues.etat
		}
		if (typeof options.newValues.collier !== 'undefined') {
			options.newValues.collier = options.newValues.collier
		}

		db.update ({
			collection: collection,
			onlyOne: options.onlyOne,
			query: options.query,
			newValues: options.newValues
		}, callback)
	},

	delete: function (options, callback) {
		db.delete ({
			collection: collection,
			onlyOne: options.onlyOne,
			query: options.query
		}, callback)
	},
}

/*

Signalement.create (options[, callback])
Signalement.create ({
	date: '27/12/16',
	creneau: '10h-11h',
	alerteur: 'toto@gmail.com',
	animal: 'Chien',
	couleur: 'Rose',
	adresse: '96 Boulevard Bessiere, 75017 Paris',
	etat: 'Bon',
	collier: true
}, function (err, result) {

})

################################################################################

Signalement.find (options[, callback])
Signalement.find ({
	query: <id> || {
		collier: true
	},
	[fields: { }]
}, function (err, docs) {

})

################################################################################

Signalement.update (options[, callback])
Signalement.update ({
	[onlyOne: true,]											// default false
	query: <id> || { },
	newValues: {
		date: '27/12/16',
		creneau: '10h-11h',
		alerteur: 'toto@gmail.com',
		animal: 'Chien',
		couleur: 'Rose',
		adresse: '96 Boulevard Bessiere, 75017 Paris',
		etat: 'Bon',
		collier: true
	}
}, function (err, result) {

})

################################################################################

Signalement.delete (options[, callback])
Signalement.delete ({
	[onlyOne: true,]											// default false
	query: <id> || { }
}, function (err, docs) {

})

*/
