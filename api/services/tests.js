require ('colors')

module.exports =	{
	parse: function (title, logs) {
		var msg = ''
		var errors = null
		for (var log in logs) {
			if (logs.hasOwnProperty(log)) {
				msg += `[${logs[log].type.cyan} ${logs[log].error ? 'FAIL'.red : 'OK'.green}]`
				if (logs[log].error) {
					if (!errors) {
						errors = `  ${logs[log].type}: ${logs[log].message}`
					} else {
						errors += `\n  ${logs[log].type}: ${logs[log].message}`
					}
				}
			}
		}
		console.log (`${title.toUpperCase ().cyan}: ${msg}`)
		if (errors) {
			console.log (errors)
		}
	},
}
