var mail =			require ('nodemailer')
var log =			require ('./log')
var transporter =	null

module.exports =	{
	init: function (options, callback) {
		if (!options || !options.user || !options.password) {
			log ('error', '|options| should contain |.user| and |.password|', callback)
			return this
		}

		transporter = mail.createTransport (`smtps://${options.user}%40gmail.com:${options.password}@smtp.gmail.com`)

		if (callback) {
			callback (null, this)
		}
		return this
	},

	send: function (options, callback) {
		if (!transporter) {
			return log ('error', `You must use mail.init () before sending mail.`)
		}

		transporter.sendMail (options, callback)
	},
}


// var nodemailer = require('nodemailer');
//
// // create reusable transporter object using the default SMTP transport
// var transporter = nodemailer.createTransport('smtps://user%40gmail.com:pass@smtp.gmail.com');
//
// // setup e-mail data with unicode symbols
// var mailOptions = {
//     from: '"Fred Foo 👥" <foo@blurdybloop.com>', // sender address
//     to: 'bar@blurdybloop.com, baz@blurdybloop.com', // list of receivers
//     subject: 'Hello ✔', // Subject line
//     text: 'Hello world 🐴', // plaintext body
//     html: '<b>Hello world 🐴</b>' // html body
// };
//
// // send mail with defined transport object
// transporter.sendMail(mailOptions, function(error, info){
//     if(error){
//         return console.log(error);
//     }
//     console.log('Message sent: ' + info.response);
// });
