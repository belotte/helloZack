var infos =			require ('../../config/infos')

module.exports =	{
	read: function (req, res) {
		if (!req.params.info || typeof infos[req.params.info] === 'undefined') {
			return res.end ()
		}
		if (typeof req.params.index != 'undefined') {
			if (typeof req.params.key != 'undefined') {
				return res.json (infos[req.params.info][req.params.index][req.params.key])
			}
			return res.json (infos[req.params.info][req.params.index])
		}
		return res.json (infos[req.params.info])
	}
}
