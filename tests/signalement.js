var db =			require ('../api/services/db')
var Signalement =	require ('../api/models/signalement')
var logs =			[]

db.connect ({
	host: '127.0.0.1',
	port: 27017,
	name: 'helloZack'
}, function (err, database) {
	Signalement.create ({
		date: '27/11/12',
		creneau: '12h-13h',
		alerteur: 'fbellott@student.42.fr',
		animal: 'chien',
		couleur: 'rouge',
		adresse: 'ici',
		etat: 'bon',
		collier: true
	}, function (err, result) {
		logs.push ({
			type: 'create',
			error: !!err || !result || result.insertedCount != 1,
			message: err ? err : `created ${result.insertedCount} rows instead of 1.`
		})

		Signalement.find ({}, function (err, docs) {
			logs.push ({
				type: 'find',
				error: !!err || !result || docs.length != 1,
				message: err ? err : `found ${docs.length} rows instead of 1.`
			})

			Signalement.update ({
				query: {
					animal: 'chien'
				},
				newValues: {
					animal: 'chat',
				}
			}, function (err, modified) {
				logs.push ({
					type: 'create',
					error: !!err || !result || modified.modifiedCount != 1,
					message: err ? err : `modified ${modified.modifiedCount} rows instead of 1.`
				})

				Signalement.delete ({
					query: {

					},
					onlyOne: true
				}, function (err, deleted) {
					logs.push ({
						type: 'delete',
						error: !!err || !docs || deleted.deletedCount != 1,
						message: err ? err : `deleted ${deleted.deletedCount} rows instead of 1`
					})

					database.close ()
					require ('../api/services/tests').parse ('Signalement', logs)
				})
			})
		})
	})
})
