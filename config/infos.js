module.exports = {
	admin: {
		nom: 'Frank',
		mail: 'belotte1355@gmail.com'
	},
	status: [{
		nom: 'Signalé',
		message: 'Une alerte a été faite et le signalement bient d\'apparaitre sur le dashboard',
	},
	{
		nom: 'Assigné',
		message: 'Une brigade a été assignée au signalement',
	},
	{
		nom: 'Sauvé',
		message: 'La brigade est intervenue et a sauvé l\'animal',
	},
	{
		nom: 'Echec',
		message: 'La brigade est intervenue et n\'a pas sauvé l\'animal',
	},
	{
		nom: 'Annulé',
		message: 'Signalment annulé par l\'operateur'
	}],
	brigades: [{
		nom: '75015-A',
		mail: 'belotte1355@gmail.com'
	}, {
		nom: '75015-B',
		mail: 'belotte1355@gmail.com'
	}, {
		nom: '75002',
		mail: 'belotte1355@gmail.com'
	}, {
		nom: '75001',
		mail: 'belotte1355@gmail.com'
	}, {
		nom: '75006-C',
		mail: 'belotte1355@gmail.com'
	}],
	etats: ['Très faible', 'Faible', 'Moyen', 'Bon'],
	animaux: ['Chat', 'Chien', 'Lapin', 'Perroquet'],
}
